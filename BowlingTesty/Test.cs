﻿using NUnit.Framework;
using System;
using BowlingGame2;
namespace BowlingTesty
{
    [TestFixture()]
    public class Test
    {
        Game game;
        [SetUp()]
        public void CreateGame()
        {
            game = new Game();
        }
        public void RollMany(int rolls , int pins)
        {
            for (int i = 0; i < rolls;i++)
            {
                game.roll(pins);
            }
        }
        [Test]
        public void RollGutterGame()
        {

            RollMany(20, 0);
            Assert.That(game.Score(), Is.EqualTo(0));
        }
        [Test]
    public void RollOnes()
        {

            RollMany(20, 1);
            Assert.That(game.Score(), Is.EqualTo(20));
        }
        [Test]
        public void FirstFrameSpare()
        {
            game.roll(5);
            game.roll(5);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(29));
        }
        [Test]
        public void FirstFrameStrike()
        {
            game.roll(10);
            RollMany(19, 1);
            Assert.That(game.Score(), Is.EqualTo(30));
        }
        [Test]
        public void PerfectGame()
        {
            RollMany(12, 10);
            Assert.That(game.Score(), Is.EqualTo(300));
        }
        [Test]
        public void TypicalGame()
        {
            game.roll(10);
            game.roll(9); game.roll(1);
            game.roll(5); game.roll(5);
            game.roll(7); game.roll(2);
            game.roll(10);
            game.roll(10);
            game.roll(10);
            game.roll(9); game.roll(0);
            game.roll(8); game.roll(2);
            game.roll(9); game.roll(1); game.roll(10);
            Assert.That(game.Score(), Is.EqualTo(187));
        }
        [Test]
        public void TwoStrike()
        {
            game.roll(10);
            game.roll(10);
            RollMany(18, 1);
            Assert.That(game.Score(), Is.EqualTo(49));
        }
        [Test]
        public void RollTwos()
        {
            RollMany(20, 2);
            Assert.That(game.Score(), Is.EqualTo(40));
        }


}
}
